import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import{HttpClientModule} from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginformComponent } from './loginform/loginform.component';
import { RegisterformComponent } from './registerform/registerform.component';
import { AdminComponent } from './loginform/admin/admin.component';
import { FileUploadComponent } from './loginform/admin/file-upload/file-upload.component';
import { ShoppingcartComponent } from './loginform/user/shoppingcart/shoppingcart.component';
import { SearchComponent } from './search/search.component';
import { SearchFilterPipe } from './SearchPipes/search-filter.pipe';
import { HeaderComponent } from './header/header.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { ProductsComponent } from './ecommerce/products/products.component';
import { OrdersComponent } from './ecommerce/orders/orders.component';
import { ShoppingCartComponent } from './ecommerce/shopping-cart/shopping-cart.component';
import { EditProductComponent } from './loginform/admin/edit-product/edit-product.component';
@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    LoginformComponent,
    RegisterformComponent,
    AdminComponent,
    FileUploadComponent,
    ShoppingcartComponent,
    SearchComponent,SearchFilterPipe, HeaderComponent, EcommerceComponent, ProductsComponent, OrdersComponent, ShoppingCartComponent, EditProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
