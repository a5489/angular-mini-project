import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { UserserviceService } from '../Service/userservice.service';
@Component({
  selector: 'app-registerform',
  templateUrl: './registerform.component.html',
  styleUrls: ['./registerform.component.css']
})
export class RegisterformComponent implements OnInit {


  user = {
    userId:"",
    userFullName:"",
    user_Gender:"",
    email:"",
    user_PhoneNumber:0,
    user_Password:""
  }
  userService: any;
 

  constructor( private route:Router,userService: UserserviceService) { this.userService=userService}

  ngOnInit(): void {
  }
  RegisterUser(loginForm1:NgForm){
    this.user.userFullName=loginForm1.value.fullname;
    this.user.email=loginForm1.value.email;
    this.user.user_PhoneNumber=loginForm1.value.user_PhoneNumber;
    this.user.userId=loginForm1.value.username;
    this.user.user_Password=loginForm1.value.password;
    this.user.user_Gender=loginForm1.value.gender;
    console.log(this.user)
    this.userService.createUser(this.user).subscribe(
      (response:any) =>
      {
         
         if (response == null)
         alert("try again");
         else
         {
           console.log("user details ", response)
           alert("registered Successfully");
    
           this.route.navigate(['login'])
    
         }
         
      },
      (error:HttpErrorResponse)=>{
        console.log(error.message);
      
      
         alert("Something error enter again");
         this.route.navigateByUrl("/register");
      }
    );
  }
}
