import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserserviceService } from '../Service/userservice.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Output,EventEmitter } from '@angular/core';
@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {
  userService: any;
//  com:boolean=true;
@Output() loggedType  = new EventEmitter<string>();
change(value: string) {
  this.loggedType.emit(value);
}

  constructor( private route:Router,userService: UserserviceService) {
    
    this.userService=userService}

  ngOnInit(): void {
  }

  
LoginUser(loginform:NgForm){

console.log(loginform.value);
this.userService.getUserByEMailANdPassword(loginform.value.email, loginform.value.password).subscribe(
  (response:any) =>
  {
     
     if (response == null)
     alert("User Not Found!! Please Signup");
     else
     {
       console.log("user details ", response)
       alert("Login Successfully");

       this.route.navigate(['home'])

     }
     
  },
  (error:HttpErrorResponse)=>{
     alert("User Not found!! Please Signup");
     this.route.navigateByUrl("/register");
  }
);
}

LoginAdmin(loginform2:NgForm){
  this.route.navigate(['admin']);
  console.log(loginform2.value);
  }
}

