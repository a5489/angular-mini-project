import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ApiService } from 'src/app/Service/api.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  product: Product = {
    productId: 0,
    productDescription: '',
    productPrize: 0,
    productName: '',
    productStockAvailable: 0,
    productImage: "",
    productDiscount: 0,
    productCategory: '',
    productCoupoun: 0
  };
  products: Product[] = [];
  fileToUpload: any;
  auth: string;
  prodid: number;
  imageUrl: string = "/assets/img/noimage.png";

  constructor(private route: ActivatedRoute, private api: ApiService) {
   
      this.api.getProducts().subscribe(
        res => {
          res.oblist.forEach((pro: Product) => {
            if (pro.productId == this.prodid) {
              this.product = pro;
              this.fileToUpload = pro.productImage;
            }
          });
        }
      );
    
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.prodid = params["user"];
    });
  }

  handleFileInput(event:Event) {
    const input = event.target as HTMLInputElement;
    this.fileToUpload=input.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }

  updateProd(desc:any, quan:any, price:any, prodname:any, image:any) {
    console.log(this.product.productId)
    this.api.updateProduct(desc.value, quan.value, price.value, prodname.value, this.fileToUpload, this.product.productId).subscribe((res: any) => {
      console.log(res);
    });
  }

}
