import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Product } from 'src/app/models/product';
import { ApiService } from 'src/app/Service/api.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {


  product = {
   
    productName:"",
    productPrize:"",
    productDiscount:0,
    productCategory:"",
    productCoupoun:0,
    productStockAvailable:0,
    productDescription:"",
    productImage:""
  }
  products: Product[] = [];
  fileToUpload:any;
  showAdd = false;
  auth: string;
  constructor(private api: ApiService, private router: Router) { }
  imageUrl: string = "/assets/img/noimage.png";
  ngOnInit() {
  
      this.api.getProducts().subscribe(
        res => {
          this.products = res.oblist;
        }
      );
  } 
  handleFileInput(event:Event) {
    const input = event.target as HTMLInputElement;
    this.fileToUpload=input.files[0];
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  show() {
    this.showAdd = true;
  }
  hide() {
    this.showAdd = false;
  }
  addProd(category:any,discount:any,coupoun:any,desc:any, quan:any, price:any, prodname:any, image:any) {
    // const formData: FormData = new FormData();
    // formData.append("productCategory",category);
    // formData.append("productDiscount",discount);
    // formData.append("productCoupoun",coupoun);
    // formData.append("productDescription", desc);
    // formData.append("productPrize", price);
    // formData.append("productName", prodname);
    // formData.append("productStockAvailable", quan);
    // formData.append("productImage", image);
    this.product.productCategory=category.value;
    this.product.productDiscount=discount.value;
    this.product.productCoupoun=coupoun.value;
    this.product.productDescription=desc.value;
    this.product.productPrize=price.value;
    this.product.productName=prodname.value;
    this.product.productStockAvailable=quan.value;
    this.product.productImage=image.value;
  
    this.api.addProduct(this.product).subscribe((res: { oblist: Product[]; }) => {
      this.products = res.oblist;
      alert(prodname.value +"is Added ");
    });
  }
  delProd(prodid:any) {

    this.api.deleteProduct(prodid.value).subscribe((res: { oblist: Product[]; }) => {
      this.products = res.oblist;
      this.ngOnInit();
    });
    
  }
  edit(prodid:any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "user": prodid.value
      }
    };
    this.router.navigate(["admin/edit"], navigationExtras);
  }
}


