import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Productsearch {
  productId:number;
  productName:string;
  productPrize:number;
  productDiscount:number;
  productCategory:string;
  productCoupoun:number;
  productStockAvailable:number;
  productDescription:string;
  productImage:string;
}


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  searchTerm!: string;
  productsser!: Productsearch[];
  allProducts!: Productsearch[];

  

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<Productsearch[]>('http://localhost:8009/products/products')
      .subscribe((data: Productsearch[]) => {
        this.productsser = data;
        this.allProducts = this.productsser;
      });
  }

  search(value: string): void {
    
    this.productsser = this.allProducts.filter((val) => val.productName.toLowerCase().includes(value));
    
    
  }


}
