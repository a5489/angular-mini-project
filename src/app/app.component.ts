import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nav: string;
  ChangeNav(nav:Event){
    this.nav='user';
  }
 
  title = 'shoppinghome';
 
}
