export interface Product {
    productId:number;
    productName:string;
    productPrize:number;
    productDiscount:number;
    productCategory:string;
    productCoupoun:number;
    productStockAvailable:number;
    productDescription:string;
    productImage:string;
}
