import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
const springurl="http://localhost:8009/products";
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  updateProduct( desc: string,
    quan: string, price: string, prodname: string, image: File, productid: any): Observable<any> {
    const formData: FormData = new FormData();
    formData.append("description", desc);
    formData.append("price", price);
    formData.append("productname", prodname);
    formData.append("quantity", quan);
    formData.append("file", image);
    formData.append("productId", productid);
    return this.http.put<any>(springurl, formData);
  }

  constructor(private http:HttpClient) { }
  getProducts(): Observable<any> {
    return this.http.get(springurl+"/products",{responseType:"json"});
  }
  addProduct(products:any): Observable<any> {
    

    return this.http.post<any>(springurl+"/addProduct",products);
    
  }
  deleteProduct( prodid: number) {
    return this.http.delete<any>(springurl+"/product/?="+prodid);
  }
}
