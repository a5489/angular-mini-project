import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private http:HttpClient) { }
  private restUrl:string='http://localhost:8009/users'
  

 


  createUser(user:any):Observable<any>{
      console.log(user);
      return this.http.post<any>(`${this.restUrl}/registeruser`,JSON.stringify(user),
      {
        headers:
          { 'Content-Type': 'application/json' }
      });
  }
  

  getUserById(id:any):Observable<any>{
      return this.http.get<any>(`${this.restUrl}/UserbyId/{id}`);
  }

//   updateUser(user:any){
//       return this.http.put<any>(`${this.restUrl}/updateuser`,user);
//   }

  deleteUser(id:any):Observable<any>{
      return this.http.delete<any>(`${this.restUrl}/deleteuserById/{id}`);
  }

  getAllUsers():Observable<any>{
      return this.http.get<any>(`${this.restUrl}/get`);
  }
  
  getUserByEMailANdPassword(email:string,password:string){
       return this.http.get<any>(`${this.restUrl}/UserbyEmail/${email}`);
   }
  
}
