import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  products: Product[];
  
  constructor(private api: ApiService) { 
    this.products=[];
  }

  ngOnInit(): void {
    this.api.getProducts().subscribe(
      res => {
        this.products = res;
        console.log(res);
      }
    );
   
    }}