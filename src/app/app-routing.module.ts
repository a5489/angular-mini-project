import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginformComponent } from './loginform/loginform.component';
import { RegisterformComponent } from './registerform/registerform.component';
import { AdminComponent } from './loginform/admin/admin.component';
import { ShoppingcartComponent } from './loginform/user/shoppingcart/shoppingcart.component';
import { SearchComponent } from './search/search.component';
import { UserComponent } from './loginform/user/user.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { EditProductComponent } from './loginform/admin/edit-product/edit-product.component';
const routes: Routes = [{ path: '', redirectTo: 'home', pathMatch: 'full' },{path:'home',component:HomepageComponent}
,{path:'login',component:LoginformComponent},{path:'register',component:RegisterformComponent},{path:'admin',component:AdminComponent}
,{path:'shoppingcart',component:ShoppingcartComponent},{path:'search',component:SearchComponent},{path:'user',component:UserComponent}
,{path:'ecommerce',component:EcommerceComponent},{path:'admin/edit',component:EditProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
