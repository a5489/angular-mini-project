package com.shopping.data.main.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.data.main.entities.Admin;

import com.shopping.data.main.repository.AdminRepository;


@Service
public class AdminService {

@Autowired
private AdminRepository adminRep;

public Admin getAdminByName(String name){
	return adminRep.findByAdminName(name);
}
public List<Admin> getAdmin(){
	return adminRep.findAll();
}
}
