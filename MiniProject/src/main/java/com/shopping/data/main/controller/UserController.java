package com.shopping.data.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.data.main.entities.User;
import com.shopping.data.main.services.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/users")
public class UserController {
	@Autowired
    private UserService service;
	/////////////////
	@PostMapping("/registeruser")
	public User addUser(@RequestBody User user) {
	    return service.saveUser(user);
	}

	 

	@PostMapping("/addUsers")
	public List<User> addUsers(@RequestBody List<User> products){
	    return service.saveUsers(products);
	}
	@GetMapping("/get")
	public List<User> findAllUsers(){
	    return service.getUsers();
	}
	@GetMapping("/UserbyId/{id}")
	public User findUserById(@PathVariable int id) {
	    return service.getUserById(id);
	}
	@GetMapping("/UserbyEmail/{email}")
	public User findUserByEmail(@PathVariable String email) {
	    return service.getUserByEmail(email);
	}
	@GetMapping("/UserbyName/{name}")
	public User findUserByName(@PathVariable String name) {
	    return service.getUserByName(name);
	}

	
	  @PutMapping("/update") public User updateUser(@RequestBody User u) {
	  return service.updateUser(u); }
	 
	@DeleteMapping("/deleteUser/{id}")
	public String deleteUser(@PathVariable int id)
	{
	    return service.deleteUser(id);
	}
}
