package com.shopping.data.main.entities;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonIgnore;


   
@Entity
public class OrderProduct {

	public OrderProduct() {
		super();
	}

	@EmbeddedId
    @JsonIgnore
    private OrderProductPK pk;

    @Column(nullable = false)
	private Integer quantity;

    // default constructor

    public OrderProduct(Order order, Products product, Integer quantity) {
        pk = new OrderProductPK();
        pk.setOrder(order);
        pk.setProduct(product);
        this.quantity = quantity;
    }

    @Transient
    public Products getProduct() {
        return this.pk.getProduct();
    }

    public OrderProductPK getPk() {
		return pk;
	}

	public void setPk(OrderProductPK pk) {
		this.pk = pk;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Transient
    public Double getTotalPrice() {
        return (double) (getProduct().getProductPrize() * getQuantity());
    }
	// standard getters and setters

    // hashcode() and equals() methods
}
