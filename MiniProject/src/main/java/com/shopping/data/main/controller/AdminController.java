package com.shopping.data.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.data.main.entities.Admin;

import com.shopping.data.main.services.AdminService;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/admin")
public class AdminController {
	@Autowired
    private AdminService service;
	@GetMapping("/name/{name}")
	public Admin findUserByName(@PathVariable String name) {
	    return service.getAdminByName(name);
	}
	@GetMapping("/getAllAdmins")
	public List<Admin> findAllAdmin(){
	    return service.getAdmin();
	}
}
