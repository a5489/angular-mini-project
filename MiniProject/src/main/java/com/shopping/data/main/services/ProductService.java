package com.shopping.data.main.services;
/*
 * import java.util.List;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Service;
 * 
 * import com.shopping.data.main.entities.Products; import
 * com.shopping.data.main.repository.ProductRepository;
 * 
 * @Service public class ProductService {
 * 
 * 
 * @Autowired private ProductRepository productRep;
 * 
 * public Products saveProduct(Products b) { return productRep.save(b); }
 * 
 * public List<Products> saveProducts(List<Products> b) { return
 * (List<Products>) productRep.saveAll(b); }
 * 
 * public List<Products> getProducts(){ return productRep.findAll(); }
 * 
 * public Products getProductById(int id){ return
 * productRep.findById(id).orElse(null); }
 * 
 * public Products getProductByName(String name){ return
 * productRep.findByProductName(name); } public String deleteProduct(int id) {
 * productRep.deleteById(id); return "Id"+id+"Deleted"; } public Products
 * updateProduct(Products b) { Products
 * existingProduct=productRep.findById(b.getProductId()).orElse(null);
 * existingProduct.setProductName(b.getProductName());
 * existingProduct.setProductPrize(b.getProductPrize());
 * existingProduct.setProductDiscount(b.getProductDiscount());
 * existingProduct.setProductCategory(b.getProductCategory()); return
 * productRep.save(existingProduct); }
 * 
 * 
 * 
 * }
 
 */import org.springframework.validation.annotation.Validated;

import com.shopping.data.main.entities.Products;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Validated
public interface ProductService {

    @NotNull Iterable<Products> getAllProducts();

    Products getProduct(@Min(value = 1L, message = "Invalid product ID.") long id);

    Products save(Products product);

	List<Products> saveProducts(List<Products> products);

	List<Products> getProducts();

	Products getProductById(long id);

	Products getProductByName(String name);

	Products updateProduct(Products product);

	String deleteProduct(long id);
}