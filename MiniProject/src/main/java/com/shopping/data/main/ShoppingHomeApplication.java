package com.shopping.data.main;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingHomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingHomeApplication.class, args);
	}

}
