package com.shopping.data.main.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity

@Table(name="products")
public class Products{
	
	@Id
    @Column(name="product_id")
	@GenericGenerator(name="generator",strategy="increment")
	@GeneratedValue(generator="generator")
    private Long productId;
	@Column(name="product_coupoun")
	private int productCoupoun;
	@Column(name="product_Stock_Available")
	private int productStockAvailable;
    @Column(name="product_name")
    private String productName;
    @Column(name="product_description")
    private String productDescription;
	@Column(name="product_prize")
    private float productPrize;
    @Column(name="product_discount")
    private float productDiscount;
    @Column(name="product_category")
    private String productCategory;
    @Column(name="product_image")
    private String productImage;
	
	public Long getProductId() {
		return productId;
	}
	
	 public Products() {
			super();
		}


	public Products(Long productId, int productCoupoun, int productStockAvailable, String productName,
			String productDescription, float productPrize, float productDiscount, String productCategory,
			String productImage) {
		super();
		this.productId = productId;
		this.productCoupoun = productCoupoun;
		this.productStockAvailable = productStockAvailable;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productPrize = productPrize;
		this.productDiscount = productDiscount;
		this.productCategory = productCategory;
		this.productImage = productImage;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public float getProductPrize() {
		return productPrize;
	}
	public void setProductPrize(float productPrize) {
		this.productPrize = productPrize;
	}
	public float getProductDiscount() {
		return productDiscount;
	}
	public void setProductDiscount(float productDiscount) {
		this.productDiscount = productDiscount;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public int getProductCoupoun() {
		return productCoupoun;
	}

	public void setProductCoupoun(int productCoupoun) {
		this.productCoupoun = productCoupoun;
	}

	public int getProductStockAvailable() {
		return productStockAvailable;
	}

	public void setProductStockAvailable(int productStockAvailable) {
		this.productStockAvailable = productStockAvailable;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getProductImage() {
		return this.productImage;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
   // @OneToMany(fetch=FetchType.LAZY, targetEntity=Student.class,cascade=CascadeType.ALL)  
   // @JoinColumn(name = "batch_id", referencedColumnName="batch_id")  
}
