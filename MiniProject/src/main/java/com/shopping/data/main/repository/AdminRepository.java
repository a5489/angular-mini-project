package com.shopping.data.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopping.data.main.entities.Admin;


public interface AdminRepository extends JpaRepository<Admin,Integer> {

	Admin findByAdminName(String name);

}
