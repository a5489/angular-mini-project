package com.shopping.data.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopping.data.main.entities.Order;


public interface OrderRepository extends JpaRepository<Order,Long> {

}
