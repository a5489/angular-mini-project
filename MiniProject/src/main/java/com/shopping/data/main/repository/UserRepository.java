package com.shopping.data.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopping.data.main.entities.User;

public interface UserRepository extends JpaRepository<User,	Integer> {

	User findByUserFullName(String name);
	User findByEmail(String name);
//	User findByEmailAndPassword(String email, String Password);
}
