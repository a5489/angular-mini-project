package com.shopping.data.main.controller;
import java.util.List;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopping.data.main.entities.Products;
import com.shopping.data.main.services.ProductService;
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService service;
@PostMapping("/addProduct")
public Products addProduct(@RequestBody Products product) {
    return service.save(product);
}
@PostMapping("/addProducts")
public List<Products> addProducts(@RequestBody List<Products> products){
    return service.saveProducts(products);
}
@GetMapping("/products")
public List<Products> findAllProducts(){
    return service.getProducts();
}
@GetMapping("/product/{id}")
public Products findProductById(@PathVariable int id) {
    return service.getProductById(id);
}
@GetMapping("/productby/{name}")
public Products findProductByName(@PathVariable String name) {
    return service.getProductByName(name);
}
@PutMapping("/update")
public Products updateProduct(@RequestBody Products product) {
    return service.updateProduct(product);
}
@DeleteMapping("/delete/{id}")
public String deleteProduct(@PathVariable int id)
{
    return service.deleteProduct(id);
}
}

 
