package com.shopping.data.main.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopping.data.main.entities.Products;
@Repository
public interface ProductRepository  extends JpaRepository<Products,Long>{
	Products findByProductName(String name);
}
