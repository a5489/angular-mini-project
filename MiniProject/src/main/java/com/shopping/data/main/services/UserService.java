package com.shopping.data.main.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.data.main.entities.User;

import com.shopping.data.main.repository.UserRepository;

@Service
public class UserService {

@Autowired
private UserRepository userRep;

public User saveUser(User b) {
	return userRep.save(b);
}

public List<User> saveUsers(List<User> b) {
	return (List<User>) userRep.saveAll(b);
}

public List<User> getUsers(){
	return userRep.findAll();
}

public User getUserById(int id){
	return userRep.findById(id).orElse(null);
}

/*
 * public User findUserDetailsByEmailAndPassword(String email, String password)
 * {
 * 
 * return userRep.findByEmailAndPassword(email, password); }
 */
public User getUserByName(String name){
	return userRep.findByUserFullName(name);
}
public User getUserByEmail(String name){
	return userRep.findByEmail(name);
}
public String deleteUser(int id) {
	userRep.deleteById(id);
    return "Id"+id+"Deleted";
}
	  public User updateUser(User b) {
		  User existingUser=userRep.findById(b.getUserId()).orElse(null);
	  existingUser.setUserFullName(b.getUserFullName());
	  existingUser.setUser_Password(b.getUser_Password());
	  return existingUser;
	  
	  }
	 

}
