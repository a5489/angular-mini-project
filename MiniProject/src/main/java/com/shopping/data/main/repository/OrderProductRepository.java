package com.shopping.data.main.repository;
import org.springframework.data.repository.CrudRepository;

import com.shopping.data.main.entities.OrderProduct;
import com.shopping.data.main.entities.OrderProductPK;

public interface OrderProductRepository extends CrudRepository<OrderProduct, OrderProductPK> {
}