package com.shopping.data.main.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;



@Entity

@Table(name="users")
public class User {
	@Id
	@Column(name="user_name")
    private int userId;
	@Column(name="user_fullname")
    private String userFullName;
	@Column(name="user_email")
    private String email;
	@Column(name="user_phonenumber")
    private String user_PhoneNumber;
	@Column(name="user_password")
    private String user_Password;
	@Column(name="user_gender")
    private String user_Gender;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserFullName() {
		return userFullName;
	}
	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUser_PhoneNumber() {
		return user_PhoneNumber;
	}
	public void setUser_PhoneNumber(String user_PhoneNumber) {
		this.user_PhoneNumber = user_PhoneNumber;
	}
	public String getUser_Password() {
		return user_Password;
	}
	public void setUser_Password(String user_Password) {
		this.user_Password = user_Password;
	}
	public String getUser_Gender() {
		return user_Gender;
	}
	public void setUser_Gender(String user_Gender) {
		this.user_Gender = user_Gender;
	}
}
