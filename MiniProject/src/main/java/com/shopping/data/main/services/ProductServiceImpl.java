package com.shopping.data.main.services;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopping.data.main.customException.ResourceNotFoundException;
import com.shopping.data.main.entities.Products;
import com.shopping.data.main.repository.ProductRepository;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Iterable<Products> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Products getProduct(long id) {
        return productRepository
          .findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("Product not found")) ;
    }

    @Override
    public Products save(Products product) {
        return productRepository.save(product);
    }
    
    public Products saveProduct(Products b) {
        return productRepository.save(b);
    }

     
    @Override
    public List<Products> saveProducts(List<Products> b) {
        return (List<Products>) productRepository.saveAll(b);
    }

     
    @Override
    public List<Products> getProducts(){
        return productRepository.findAll();
    }

    @Override

    public Products getProductById(long id){
        return productRepository.findById(id).orElse(null);
    }

     
    @Override
    public Products getProductByName(String name){
        return productRepository.findByProductName(name);
    }
    @Override
    public String deleteProduct(long id) {
        productRepository.deleteById(id);
        return "Id"+id+"Deleted";
    }
    @Override
    public Products updateProduct(Products b) {
        Products existingProduct=productRepository.findById(b.getProductId()).orElse(null);
        existingProduct.setProductName(b.getProductName());
        existingProduct.setProductPrize(b.getProductPrize());
        existingProduct.setProductDiscount(b.getProductDiscount());
        existingProduct.setProductCategory(b.getProductCategory());
        return productRepository.save(existingProduct);
}}